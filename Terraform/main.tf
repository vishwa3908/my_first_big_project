
provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.aws_region
}

#1. Create VPC
resource "aws_vpc" "prod-vpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "Production"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.prod-vpc.id

}

#3. Create Custom Route Table

resource "aws_route_table" "prod-rt" {
  vpc_id = aws_vpc.prod-vpc.id
  #To do default route, change the cidr block to "0.0.0.0/0"
  #Default route means that the traffic will be routed to the internet gateway
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block = "::/0" #"::/0" is the default
    gateway_id      = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "Production Route Table"
  }
}

#4. Create Subnet
resource "aws_subnet" "subnet-1" {
  vpc_id            = aws_vpc.prod-vpc.id
  cidr_block        = var.subnet_cidr
  availability_zone = "us-east-1a"

  tags = {
    Name = "Prod-Subnet-1"
  }
}

resource "aws_subnet" "rds_subnet" {
  vpc_id     = aws_vpc.prod-vpc.id
  cidr_block = "10.0.128.0/18"
  availability_zone = "us-east-1a"
}

resource "aws_subnet" "rds_subnet1" {
  vpc_id     = aws_vpc.prod-vpc.id
  cidr_block = "10.0.192.0/18"
  availability_zone = "us-east-1b"
}

#5. Associate Route Table with Subnet

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.prod-rt.id
}

#6. Create Security Group to allow port 22,80,443
resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow Web inbound traffic"
  vpc_id      = aws_vpc.prod-vpc.id

  #ingress can be used to allow traffic from the internet
  #ingress dictates the traffic that is allowed to the security group
  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"] #cidr block here clamp down on what subnets can access the port
    #since we are creating a web server that is meant to use by everyone, we need to allow all traffic n hence 0.0.0.0/0
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80 #http resides on port 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  #egresss direction is used to allow traffic from the subnet to the internet
  egress { # Deny all outbound traffic
    from_port   = 0
    to_port     = 0
    protocol    = "-1" # -1 is used to allow all protocols
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "allow_web"
  }
}

#7. Create a network interface with an ip in the subnet that was created in step 4
resource "aws_network_interface" "web-server-nic" {
  subnet_id       = aws_subnet.subnet-1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_web.id]

}

#8. Assign an elastic IP to the network interface
#Elastic IP is public IP address that is assigned to the network interface

resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.web-server-nic.id
  associate_with_private_ip = "10.0.1.50"
  depends_on                = [aws_internet_gateway.gw,aws_instance.web-server-instance]

   #AWS EIP relies on deployment of Internet Gateway
  #depends_on is used to make sure that the internet gateway is deployed before the elastic ip
  # and here we will not use .id since we want the entire object
}

resource "aws_key_pair" "demokey" {
  key_name   = var.key_name
  public_key = file(var.public_key)
}

# Get latest Ubuntu Linux Focal Fossa 20.04 AMI
data "aws_ami" "ubuntu-linux-2004" {
  most_recent = true
  owners      = ["099720109477"] # Canonical
  
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

#9. Create Ubuntu server and install/enable apache2
#ami is the image that we will use to create the instance
resource "aws_instance" "web-server-instance" {
  ami          = data.aws_ami.ubuntu-linux-2004.id #Grab AMI from console
  instance_type = "t2.micro"
  key_name      = aws_key_pair.demokey.id #to refrence the key pair we created earlier
  #always specify the availability zone whenever get the option and use the same availability zone

  network_interface {
    device_index = 0
    #device_index is used to specify the network interface
    network_interface_id = aws_network_interface.web-server-nic.id
  }

  tags = {
    Name = "web-server"
  }

  user_data = <<EOF
#!/bin/bash -xe
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
sudo yum update -y
sudo amazon-linux-extras install nginx1 -y
sudo su -c "/bin/echo 'HELLO' >/usr/share/nginx/html/index.html"
instance_ip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
sudo su -c "echo $instance_ip >>/usr/share/nginx/html/index.html"
sudo systemctl start nginx
sudo systemctl enable  nginx
EOF
}

output "server_public_ip" {
  value = aws_eip.one.public_ip
}

output "server_private_ip" {
  value = aws_instance.web-server-instance.private_ip
}

output "server_id" {
  value = aws_instance.web-server-instance.id
}

resource "aws_db_subnet_group" "default" {
  name       = "db__group"
  subnet_ids = [aws_subnet.rds_subnet.id,aws_subnet.rds_subnet1.id]

  tags = {
    Name = "My DB subnet group"
  }
}

#terraform destroy -target aws_instance.web-server-instance
# By using the -target flag, terraform will only destroy the resources that are specified in the target
resource "aws_security_group" "rds_sg" {
  name        = "rds_sg"
  description = "Allow Web inbound traffic"
  vpc_id      = aws_vpc.prod-vpc.id
  ingress {
    description = "mysql"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  

  egress { # Deny all outbound traffic
    from_port   = 0
    to_port     = 0
    protocol    = "-1" # -1 is used to allow all protocols
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_db_instance" "default" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t2.micro"
  port                 = 3306
  db_subnet_group_name = aws_db_subnet_group.default.id
  db_name              = "mydb"
  username             = "vishwa"
  password             = "Password.123"
  parameter_group_name = "default.mysql8.0"
  publicly_accessible  = true
  skip_final_snapshot  = true
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
}

output "rds_endpoint" {
  value = aws_db_instance.default.endpoint
}
